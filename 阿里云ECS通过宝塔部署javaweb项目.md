# 阿里云ECS通过宝塔部署javaweb项目

这篇文章主要是用来记录自己第一次部署javaweb项目经验，以及碰到的一些问题和参考的解决方法。

## 一、前期准备

- 已经购买好的阿里云服务器
- 云ECS服务器基本原理一至（学生认证有飞天加速计划，免费领取1台ECS七个月）
- 可以购买域名、也可以不需要购买域名（域名需要备案，备案的时间很长...）
- 一个可以本地运行的javaweb项目

## 二、登录阿里云控制台

地址：https://www.aliyun.com/

开启实例

配置打开防火墙安全组里开启相关端口（非常重要）

## 三、宝塔的安装及使用

### 1.进入宝塔主页复制不同环境下的安装指令
地址：https://www.bt.cn/bbs/thread-19376-1-1.html

### 2.点击远程连接弹出窗口后输入绿色的sudo su root 切换到root账号 

(已经在root下不需要)

### 3.开始安装

- 将刚才复制的安装代码复制到黑框内执行（切记linux中不支持cv大法，鼠标右键，选择粘贴、按下enter）

- 安装过程出现停止都选择y或者yes

- 安装完成后会显示两个地址和用户名以及密码

### 4.访问宝塔

- 将外网面板复制到浏览器地址栏即可访问

- 输入刚才生成的用户名和密码即可登录成功（面板设置里可以改用户名和密码）

### 5.进入软件商店安装tomcat、mysql

这里注意各软件的版本与自己开发的环境一致

### 6.开放一些必要的端口

### 7.创建数据库

设置用户名和密码，注意在自己项目里更改相关MyBatis配置

### 8.导入数据库文件

### 9.进入文件下的tomcat下的webapps下

（后面需要将打包好的项目文件放在这个目录下）

## 四、本地打包Javaweb项目

- 想要打包成war包需要在pow文件中添加以下这一行

  ```xml
  <packaging>war</packaging>
  ```

- 在SpringBoot同级目录下添加一下配置类

- 依次点击idea右侧栏的Maven-->clean-->package
- 在target目录下会生成相应的war包

## 五、成功部署

### 1.在宝塔页面把项目上传到tomcat的webapps下

（刷新页面会自动解压）

### 2.项目访问

在浏览器地址栏输入：ip:8080/项目名      （即可访问）

其中8080为端口号，根据自己为tomcat设置的端口号进行更改

## 六、遇到的问题

### 1.Tomcat部署后，页面访问不到静态资源

问题描述：javaweb项目，tomcat发布，自己本机一切正常，但在阿里云上正常部署完成项目后，启动成功，也

可以访问到猫界面，但是访问自己的vue前端时，页面空白，在浏览器f12发现是静态资源未引进的问题。

解决方法：

- 1.打开tomcat目录下conf文件中server.xml文件

- 2.在文件中添加这一行代码

  ```xml
    <Context path="" docBase="" reloadable="true" />
    <!--docBase路径为本tomcat中webapps下war包生成的名字相应的文件夹-->
  ```

- 3.重启tomcat，再次访问，静态文件即可引用成功

具体原因分析参考：<https://blog.csdn.net/qq_21033663/article/details/52825258>

### 2.解析 HTTP 请求 header 错误注意：HTTP请求解析错误的进一步发生将记录在DEBUG级别。

问题描述：运行tomcat后日志文件报错，1.解析 HTTP 请求 header 错误注意：HTTP请求解析错误的进一步发生

将记录在DEBUG级别。2.在方法名称中发现无效的[字符串](https://so.csdn.net/so/search?q=字符串&spm=1001.2101.3001.7020), HTTP 方法名必须是有效的符号。

解决方法：

- 进入tomcat --> conf--> server.xml，找到此文件，用记事本打开。找到以下代码，大概在69行

  ```xml
      <Connector port="8080" protocol="HTTP/1.1"
                 connectionTimeout="20000"
                 redirectPort="8443" 
  			   URIEncoding="utf-8" 
      <!-- A "Connector" using the shared thread pool-->
  ```

- 修改为：

  ```xml
  <Connector port="8080" protocol="HTTP/1.1"
                 connectionTimeout="20000"
                 redirectPort="8443" 
  			   URIEncoding="utf-8" 
  			   relaxedPathChars="|{}[],%"
  			   relaxedQueryChars="|{}[],%"/>
      <!-- A "Connector" using the shared thread pool-->
  ```

### 3.其他可能会遇到的问题及解决方法

- 关闭防火墙
- kill杀死被占用的进程
- 赋予项目文件777权限
- 将请求头https改成http
- 终端下使用**nohup java -jar xxx.jar &** 运行jar包

## 七、参考

[1]<https://blog.csdn.net/qq_21033663/article/details/52825258>

[2]<https://blog.csdn.net/weixin_57665275/article/details/124164360>

[3]<https://blog.csdn.net/weixin_44479107/article/details/124943149>

[4]<https://blog.csdn.net/m0_37911124/article/details/120733105>

[5]<https://blog.csdn.net/yuanchangliang/article/details/114872868>

[6]<https://blog.csdn.net/yangbukong/article/details/119379388>